Wunnel
======

<!--
SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.
For more information, please refer to <https://unlicense.org/>
SPDX-License-Identifier: Unlicense
-->

_Try it online_ [@ catseye.tc](https://catseye.tc/installation/Wunnel)
| _Wiki entry_ [@ esolangs.org](https://esolangs.org/wiki/Wunnel)
| _See also:_ [Etcha](https://codeberg.org/catseye/Etcha#the-etcha-programming-language)
∘ [Gemooy](https://codeberg.org/catseye/Gemooy#gemooy)

- - - -

This is the reference distribution of Wunnel, an esoteric programming
language.

At present, it contains:

*   the specification of the language, in [doc/Wunnel.md][], copied from
    the [Wunnel article][] on the esolangs.org wiki (which is in the
    public domain and was mostly written by me anyway);
*   an implementation of Wunnel in JavaScript and HTML5, using [yoob.js][]; and
*   an implementation of Wunnel in Lua.

It's all in the public domain; see the file [LICENSES/Unlicense.txt][] for more information.

There is also a public-domain implementation of Wunnel in Java included
in [the yoob distribution][].

[doc/Wunnel.md]: doc/Wunnel.md
[Wunnel article]: http://esolangs.org/wiki/Wunnel
[yoob]: http://catseye.tc/node/yoob
[yoob.js]: http://catseye.tc/node/yoob.js
[LICENSES/Unlicense.txt]: LICENSES/Unlicense.txt
[the yoob distribution]: https://catseye.tc/distribution/yoob_distribution
