--
-- debugging.lua
-- Generic debugging facilities for Lua
--
-- SPDX-FileCopyrightText: In 2024, Chris Pressey, the original author of this work, placed it into the public domain.
-- For more information, please refer to <https://unlicense.org/>
-- SPDX-License-Identifier: Unlicense
--

local debug_the_things = ""

local debug_these = function(the_things)
    debug_the_things = the_things
end

local do_debug = function(what, fn)
    if debug_the_things:find(what) then
        fn()
    end
end

local debug = function(what, s)
    do_debug(what, function() print("--> (" .. s .. ")") end)
end

local render
render = function(v, count)
    if not count then count = 0 end
    if count > 100 then return "!!OVERFLOW!!" end
    if type(v) == "table" then
        local s = "{"
        local key, value
        -- FIXME it would be great if this could be sorted by key.
        -- alas, in Lua that will take a little work.
        for key, value in pairs(v) do
            s = s .. render(key, count + 1) .. ": " .. render(value, count + 1) .. ","
        end
        return s .. "}"
    else
        return tostring(v)
    end
end

return {
    do_debug = do_debug,
    debug = debug,
    debug_these = debug_these,
    render = render
}
