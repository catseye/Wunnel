--
-- wunnel.lua
-- Wunnel interpreter in Lua
--
-- SPDX-FileCopyrightText: In 2024, Chris Pressey, the original author of this work, placed it into the public domain.
-- For more information, please refer to <https://unlicense.org/>
-- SPDX-License-Identifier: Unlicense
--

local Debugging = require "debugging"
local Playfield = require "playfield"
local Tape = require "tape"

--
-- Utils
--

local rotate_ccw = function(dx, dy)
    if dx == 0 and dy == -1 then
        return -1, -1
    elseif dx == -1 and dy == -1 then
        return -1, 0
    elseif dx == -1 and dy == 0 then
        return -1, 1
    elseif dx == -1 and dy == 1 then
        return 0, 1
    elseif dx == 0 and dy == 1 then
        return 1, 1
    elseif dx == 1 and dy == 1 then
        return 1, 0
    elseif dx == 1 and dy == 0 then
        return 1, -1
    elseif dx == 1 and dy == -1 then
        return 0, -1
    end
end

local cursor_advance = function(c)
    c.x = c.x + c.dx
    c.y = c.y + c.dy
end

local in_bounds = function(p, x, y)
    -- ?
    return x > -100 and x < 100 and y > -100 and y < 100
end

local positive_genus = "0689@%&QROPADBqeopadb"

local function genus_more_than_zero(c)
    for i = 1, #positive_genus do
        if positive_genus:sub(i, i) == c then
            return true
        end
    end
    return false
end

local create_operation_table = function()
    local optab = Playfield.new()

    local table =
        "RRS-+N\n" ..
        "<S>0N0\n" ..
        ">I<N+-\n" ..
        "NOSS<E\n" ..
        "SEN>SE\n" ..
        "RNRRRR\n"
    local map = {
        ["R"] = "ROT",
        ["S"] = "SHU",
        ["N"] = "NOP",
        ["+"] = "PLU",
        ["-"] = "NEG",
        ["0"] = "BLA",
        ["<"] = "LEF",
        [">"] = "RIG",
        ["E"] = "END",
        ["O"] = "OUT",
        ["I"] = "INP"
     }
    optab:load(table, function(c, x, y)
        return map[c]
    end)
    return optab
end

--
-- Single Step
--

function step(p, ip, optab, opp, tape, cio)
    local instruction = p:peek(ip.x, ip.y)
    local k = optab:peek(opp.x, opp.y)

    Debugging.debug(
        "state", "instr: '" .. instruction .. "', semantic: '" .. k .. "'"
    )

    if genus_more_than_zero(instruction) then
        if k == 'END' then
            return 'stop'
        elseif k == 'NOP' then
            -- Do nothing
        elseif k == 'SHU' then
            if ip.dx == -1 and ip.dy == 0 then
                ip.y = ip.y - tape:read()
            elseif ip.dx == 1 and ip.dy == 0 then
                ip.y = ip.y + tape:read()
            elseif ip.dx == 0 and ip.dy == -1 then
                ip.x = ip.x + tape:read()
            elseif ip.dx == 0 and ip.dy == 1 then
                ip.x = ip.x - tape:read()
            end
        elseif k == 'ROT' then
            -- really, opp's dx & dy are not independent of ip's.
            -- we make sure to keep them in sync here.
            ip.dx, ip.dy = rotate_ccw(ip.dx, ip.dy)
            ip.dx, ip.dy = rotate_ccw(ip.dx, ip.dy)
            opp.dx, opp.dy = rotate_ccw(opp.dx, opp.dy)
            opp.dx, opp.dy = rotate_ccw(opp.dx, opp.dy)
        elseif k == 'LEF' then
            tape:move_left()
        elseif k == 'RIG' then
            tape:move_right()
        elseif k == 'NEG' then
            tape:write(-1)
        elseif k == 'BLA' then
            tape:write(0)
        elseif k == 'PLU' then
            tape:write(1)
        elseif k == 'OUT' then
            cio.output = cio.output .. (tape:read() == 0 and '0' or '1')
        elseif k == 'INP' then
            local c = cio.input:sub(1, 1)
            if c == '' then
                return 'block'
            end
            tape:write(c == '1' and 1 or 0)
            cio.input = cio.input:sub(2)
        end
    else
        cursor_advance(opp)
        opp.x = opp.x % 6
        opp.y = opp.y % 6
    end

    cursor_advance(ip)
    if not in_bounds(p, ip.x, ip.y) then
        return 'stop'
    end
end


--
-- Loader
--

local load_file = function(filename)
    local f = assert(io.open(filename, "r"))
    local program_text = f:read("*all")
    f:close()
    return program_text
end

--
-- Main
--

function main(arg)
    math.randomseed(os.time())
    local filename
    while #arg > 0 do
        if arg[1] == "--debug" then
            table.remove(arg, 1)
            Debugging.debug_these(arg[1])
        elseif arg[1] ~= "" then
            filename = arg[1]
        end
        table.remove(arg, 1)
    end

    --
    -- Program state
    --
    local p = Playfield.new()

    local program_text = load_file(filename)
    p:load(program_text, function(c, x, y)
        return c
    end)

    local done = false

    local ip = {
        ["x"] = 0,
        ["y"] = 0,
        ["dx"] = 0,
        ["dy"] = 1,
    }
    local optab = create_operation_table()
    local opp = {
        ["x"] = 0,
        ["y"] = 0,
        ["dx"] = 0,
        ["dy"] = 1,
    }
    local tape = Tape.new()

    local cio = {
        input = "",
        output = "",
    }

    --
    -- Execution loop
    --
    while not done do
        Debugging.debug(
            "state",
            "ip.xy: (" .. tostring(ip.x) .. ", " .. tostring(ip.y) .. ") " ..
            "ip.dxdy: (" .. tostring(ip.dx) .. ", " .. tostring(ip.dy) .. ") " ..
            "opp.xy: (" .. tostring(opp.x) .. ", " .. tostring(opp.y) .. ")"
        )
        local result = step(p, ip, optab, opp, tape, cio)
        if result == 'stop' then
            done = true
        elseif result == 'block' then
            if cio.input ~= "" then
                error("Should not block when still have input")
            else
                local c = nil
                c = io.read(1)
                while c ~= "1" and c ~= "0" and not done do
                    if c == nil then
                        done = true
                    else
                        c = io.read(1)
                    end
                end
                cio.input = c
            end
        end
    end
    io.write(cio.output)
    io.write("\n")
end

if arg ~= nil then
    main(arg)
end
