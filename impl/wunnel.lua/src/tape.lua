--
-- tape.lua
-- Mutable Turing-machine-like tape
--
-- SPDX-FileCopyrightText: The authors of this work have dedicated it to the public domain.
-- For more information, please refer to <https://unlicense.org/>
-- SPDX-License-Identifier: Unlicense
--

local Tape = {}

--
-- Private function: pick the appropriate side of contents, & translate
--
local pick_side = function(self)
    if self.head > 0 then
        return self.positive_land, self.head
    else
        return self.negative_land, 1-self.head
    end
end

--
-- Write a value to the cell under the head
--
function Tape:write(value)
    local contents, x = pick_side(self)
    contents[x] = value
    if not self.min or x < self.min then self.min = x end
    if not self.max or x > self.max then self.max = x end
end

--
-- Read the value from the cell under the head
--
function Tape:read()
    local contents, x = pick_side(self)
    return contents[x] or 0
end

--
-- Move the head left by n cells (default 1)
--
function Tape:move_left(n)
    n = n or 1
    self.head = self.head - n
end

--
-- Move the head right by n cells (default 1)
--
function Tape:move_right(n)
    n = n or 1
    self.head = self.head + n
end

function Tape.new()
    local self = {
        positive_land = {},
        negative_land = {},
        min = nil,
        max = nil,
        head = 0,
    }
    setmetatable(self, { __index = Tape })
    return self
end

return Tape
